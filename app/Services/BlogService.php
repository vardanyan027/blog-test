<?php

namespace App\Services;

use App\Models\Blog;
use App\RepositoryInterface\Blog\BlogInterface;

class BlogService extends BaseService
{
    public function __construct(Blog $model, BlogInterface $baseRepository)
    {
        parent::__construct($model, $baseRepository);
    }
}
