<?php

namespace App\Services;

use App\Models\Blog;
use App\Models\News;
use App\RepositoryInterface\Blog\BlogInterface;
use App\RepositoryInterface\News\NewsInterface;

class NewsService extends BaseService
{
    public function __construct(News $model, NewsInterface $baseRepository)
    {
        parent::__construct($model, $baseRepository);
    }
}
