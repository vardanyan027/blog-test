<?php

namespace App\Services;

use App\RepositoryInterface\BaseInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseService
{
    public Model $model;
    public BaseInterface $repository;

    public function __construct(
        Model $model,
        BaseInterface $baseRepository
    )
    {
        $this->model = $model;
        $this->repository = $baseRepository;
    }

    /**
     * @return Collection
     */
    public function get(): Collection
    {
        return $this->repository->get();
    }

    /**
     * @param $dto
     * @return mixed
     */
    public function create($dto) {
        $item = $this->model->create($dto);
        $item->save();
        return $item;
    }

    /**
     * @param $id
     * @param $dto
     * @return mixed
     */
    public function update($id, $dto) {
        $item = $this->repository->getById($id);
        $item = $item->updateby($dto);
        $item->save();
        return $item;
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id) {
        return $this->repository->delete($id);
    }

    public function search(string $name) {
        return $this->repository->search('name', $name);
    }

}
