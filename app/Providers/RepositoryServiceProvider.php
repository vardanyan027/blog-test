<?php

namespace App\Providers;

use App\RepositoryInterface\Blog\BlogInterface;
use App\RepositoryInterface\Blog\BlogRepository;
use App\RepositoryInterface\News\NewsInterface;
use App\RepositoryInterface\News\NewsRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BlogInterface::class, BlogRepository::class);
        $this->app->bind(NewsInterface::class, NewsRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
