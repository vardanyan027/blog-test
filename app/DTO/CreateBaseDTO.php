<?php

namespace App\DTO;

use Illuminate\Http\Request;

abstract class CreateBaseDTO
{
    public abstract function makeRequest(Request $request);
}
