<?php

namespace App\DTO;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CreateNewsDTO extends CreateBaseDTO
{
    public $name;
    public $description;
    public $image;

    public function makeRequest(Request $request): CreateNewsDTO
    {
        $this->name = $request->get('name');
        $this->description = $request->get('description');
        $file = $request->get('image');
        Storage::disk('local')->put('public/' . $file->getClientOriginalName(), file_get_contents($file));
        $this->image = $file->getClientOriginalName();
        return $this;
    }

}
