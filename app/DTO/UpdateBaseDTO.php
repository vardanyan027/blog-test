<?php

namespace App\DTO;

use Illuminate\Http\Request;

abstract class UpdateBaseDTO
{
    public abstract function makeRequest(Request $request);
}
