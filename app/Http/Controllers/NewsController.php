<?php

namespace App\Http\Controllers;

use App\DTO\CreateBaseDto;
use App\DTO\UpdateBaseDto;
use App\Services\NewsService;

class NewsController extends BaseController
{
    public function __construct(NewsService $service, CreateBaseDto $createDto, UpdateBaseDto $updateDto)
    {
        parent::__construct($service, $createDto, $updateDto);
    }
}
