<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(LoginRequest $loginRequest)
    {
        $email = $loginRequest->get('email');
        $password = $loginRequest->get('password');

        $user = User::where('email', $email)->first();

        if(!isset($user)) {
            return back()->withErrors([
                'message' => 'user not found'
            ]);
        }

        if(Hash::check($password, $user->password)) {
            Auth::attempt([
                'email' => $email,
                'password' => $password
            ]);
            return redirect('/category');
        }

        return back()->withErrors([
            'message' => 'credentials is incorrect'
        ]);
    }

    public function register(RegisterRequest $registerRequest) {
        $name = $registerRequest->get('name');
        $email = $registerRequest->get('email');
        $password = $registerRequest->get('password');

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);

        Auth::attempt([
            'email' => $email,
            'password' => $password
        ]);

        return redirect('/blog');
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
