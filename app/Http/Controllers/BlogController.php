<?php

namespace App\Http\Controllers;

use App\DTO\CreateBlogDTO;
use App\DTO\UpdateBlogDTO;
use App\Services\BlogService;

class BlogController extends BaseController
{
    public function __construct(BlogService $service, CreateBlogDTO $createDto, UpdateBlogDTO $updateDto)
    {
        parent::__construct($service, $createDto, $updateDto);
    }
}
