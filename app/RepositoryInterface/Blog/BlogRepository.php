<?php

namespace App\RepositoryInterface\Blog;

use App\Models\Blog;
use App\RepositoryInterface\BaseRepository;

class BlogRepository extends BaseRepository implements BlogInterface
{
    public function __construct(Blog $table)
    {
        parent::__construct($table);
    }
}
