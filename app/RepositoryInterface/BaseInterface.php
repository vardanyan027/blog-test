<?php

namespace App\RepositoryInterface;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseInterface
{
    /**
     * @return Collection
     */
    public function get(): Collection;

    /**
     * @param string $key
     * @param string $value
     * @return Collection
     */
    public function search(string $key, string $value): Collection;

    /**
     * @param int $id
     * @return Model
     */
    public function getById(int $id): Model;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}
