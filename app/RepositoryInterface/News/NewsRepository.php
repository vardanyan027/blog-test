<?php

namespace App\RepositoryInterface\News;

use App\Models\Blog;
use App\RepositoryInterface\BaseRepository;

class NewsRepository extends BaseRepository implements NewsInterface
{
    public function __construct(Blog $table)
    {
        parent::__construct($table);
    }
}
