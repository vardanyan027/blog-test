<?php

namespace App\RepositoryInterface;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseInterface
{
    private Model $table;
    public function __construct(Model $table)
    {
        $this->table = $table;
    }

    public function get(): Collection
    {
        return $this->table->get();
    }

    public function search($key, $value): Collection
    {
        return $this->table->where($key, 'like', '%'.$value.'%')->get();
    }

    public function getById(int $id): Model
    {
        return $this->table->where('id', $id)->first();
    }

    public function delete(int $id): bool
    {
        $item = $this->getById($id);
        $item->delete();
    }
}
