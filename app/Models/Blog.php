<?php

namespace App\Models;

use App\DTO\CreateBlogDTO;
use App\DTO\UpdateBlogDTO;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $description
 * @property string $image
 */
class Blog extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    protected $fillable = [
        'name',
        'description',
        'image'
    ];

    public function create(CreateBlogDTO $createBlogDTO): Blog
    {
        $item = new self();
        $item->name = $createBlogDTO->name;
        $item->description = $createBlogDTO->description;
        $item->image = $createBlogDTO->image;
        return $item;
    }

    public function updateby(UpdateBlogDTO $updateBlogDTO): Blog
    {
        $item->name = $updateBlogDTO->name;
        $item->description = $updateBlogDTO->description;
        $item->image = $updateBlogDTO->image;
        return $this;
    }

}
