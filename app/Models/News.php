<?php

namespace App\Models;

use App\DTO\CreateNewsDTO;
use App\DTO\UpdateNewsDTO;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $description
 * @property string $image
 */
class News extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    protected $fillable = [
        'name',
        'description',
        'image'
    ];

    public function create(CreateNewsDTO $createBlogDTO): News
    {
        $item = new self();
        $item->name = $createBlogDTO->name;
        $item->description = $createBlogDTO->description;
        $item->image = $createBlogDTO->image;
        return $item;
    }

    public function updateby(UpdateNewsDTO $updateBlogDTO): News
    {
        $this->name = $updateBlogDTO->name;
        $this->description = $updateBlogDTO->description;
        $this->image = $updateBlogDTO->image;
        return $this;
    }
}
